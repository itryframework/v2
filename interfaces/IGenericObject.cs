namespace it.itryframework2.interfaces
{
    public interface IGenericObject
    {
        string TableName
        {
            get;
        }

        string PrimaryKey
        {
            get;
        }
    }
}
