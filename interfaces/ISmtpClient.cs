﻿using System.Collections.Generic;

namespace it.itryframework2.interfaces
{
    public interface ISmtpClient
    {
        void DoSend(string from, string fromName, string to, List<string> bcc, string subject, string body, string smtp, int port, bool ssl, string username, string password, string props);
    }
}
