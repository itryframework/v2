using System.Configuration;

namespace it.itryframework2.config
{
    public sealed class ErrorConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("managerClassName", IsRequired = true)]
        public string ManagerClassName
        {
            get { return this["managerClassName"] as string; }
        }

        [ConfigurationProperty("smtp", IsRequired = false)]
        public string Smtp
        {
            get { return this["smtp"] as string; }
        }

        [ConfigurationProperty("authenticationUser", IsRequired = false)]
        public string AuthenticationUser
        {
            get { return this["authenticationUser"] as string; }
        }

        [ConfigurationProperty("authenticationPwd", IsRequired = false)]
        public string AuthenticationPwd
        {
            get { return this["authenticationPwd"] as string; }
        }

        [ConfigurationProperty("mailFrom", IsRequired = false)]
        public string MailFrom
        {
            get { return this["mailFrom"] as string; }
        }

        [ConfigurationProperty("mailFromName", IsRequired = false)]
        public string MailFromName
        {
            get { return this["mailFromName"] as string; }
        }

        [ConfigurationProperty("mailTo", IsRequired = false)]
        public string MailTo
        {
            get { return this["mailTo"] as string; }
        }

        [ConfigurationProperty("mailBccs", IsRequired = false)]
        public string MailBCCs
        {
            get { return this["mailBccs"] as string; }
        }

        [ConfigurationProperty("smtpPort", IsRequired = false)]
        public int? SmtpPort
        {
            get
            {
                int result = 0;
                if (int.TryParse(this["smtpPort"] as string, out result))
                    return result;
                return null;
            }
        }

        [ConfigurationProperty("smtpSsl", IsRequired = false)]
        public bool? SmtpSSL
        {
            get
            {
                int result = 0;
                if (int.TryParse(this["smtpSsl"] as string, out result))
                    return result == 1;
                return null;
            }
        }

        [ConfigurationProperty("mailSubjectPrefix", IsRequired = false)]
        public string MailSubjectPrefix
        {
            get { return this["mailSubjectPrefix"] as string; }
        }

        [ConfigurationProperty("enable", IsRequired = false, DefaultValue = true)]
        public bool Enable
        {
            get
            {
                try
                {
                    bool _enable;
                    bool.TryParse(this["enable"].ToString(), out _enable);
                    return _enable;
                }
                catch { return false; }
            }
        }
    }
}
