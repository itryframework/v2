using System.Configuration;

namespace it.itryframework2.config
{
    public sealed class MailConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("smtpClientClassName", IsRequired = false)]
        public string SmtpClientClassName
        {
            get { return this["smtpClientClassName"] as string; }
        }

        [ConfigurationProperty("smtp", IsRequired = true)]
        public string Smtp
        {
            get { return this["smtp"] as string; }
        }

        [ConfigurationProperty("authenticationUser", IsRequired = false)]
        public string AuthenticationUser
        {
            get { return this["authenticationUser"] as string; }
        }

        [ConfigurationProperty("authenticationPwd", IsRequired = false)]
        public string AuthenticationPwd
        {
            get { return this["authenticationPwd"] as string; }
        }

        [ConfigurationProperty("mailFrom", IsRequired = false)]
        public string MailFrom
        {
            get { return this["mailFrom"] as string; }
        }

        [ConfigurationProperty("mailFromName", IsRequired = false)]
        public string MailFromName
        {
            get { return this["mailFromName"] as string; }
        }

        [ConfigurationProperty("mailBccs", IsRequired = false)]
        public string MailBCCs
        {
            get { return this["mailBccs"] as string; }
        }

        [ConfigurationProperty("smtpClientProps", IsRequired = false)]
        public string SmtpClientProps
        {
            get { return this["smtpClientProps"] as string; }
        }

        [ConfigurationProperty("smtpPort", IsRequired = false)]
        public string SmtpPort
        {
            get { return this["smtpPort"] as string; }
            /*
            get
            {

                int result = 0;
                if (int.TryParse(this["smtpPort"] as string, out result))
                    return result;
                return 25;
            }*/
        }

        [ConfigurationProperty("smtpSsl", IsRequired = false)]
        public string SmtpSSL
        {
            get { return this["smtpSsl"] as string; }
            /*
            get
            {
                int result = 0;
                if (int.TryParse(this["smtpSsl"] as string, out result))
                    return result == 1;
                return false;
            }*/
        }
    }
}
