using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using it.itryframework2.config;
using it.itryframework2.managers.mail;
using it.itryframework2.interfaces;

namespace it.itryframework2.managers.log
{
    public sealed class MailLogManager : ILog
    {
        #region ILog Membri di

        public bool manageError(ErrorConfigurationSection errorSection, Exception ex, string customMessage)
        {
            StringBuilder s = new StringBuilder();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            if (context != null)
            {
                s.Append("<b>Sito</b><br><u>" + context.Request.ServerVariables["SERVER_NAME"] + "</u><br>");
                s.Append("<br><b>Pagina richiesta</b><br><u>" + context.Request.RawUrl + "</u><br>");
                s.Append("<br><b>IP richiedente</b><br>" + context.Request.ServerVariables["REMOTE_ADDR"] + "<br>");
                s.Append("<br><b>Host name</b><br>" + context.Request.ServerVariables["REMOTE_HOST"] + "<br>");
            }
            s.Append("<br><b>Message</b>:<br>" + ex.Message + "<br>");
            s.Append("<br><b>Source</b>:<br>" + ex.Source + "<br>");
            s.Append("<br><b>Stack</b>:<br>" + ex.StackTrace + "<br>");
            s.Append("<br><b>CustomMessage</b>:<br>" + customMessage);

            List<string> bcc = null;
            if (!string.IsNullOrEmpty(errorSection.MailBCCs))
            {
                string[] arrBcc = errorSection.MailBCCs.Split(',');
                bcc = new List<string>(arrBcc.Where(b => !string.IsNullOrWhiteSpace(b)));
            }

            MailManager.Instance.Send(
                errorSection.MailFrom,
                errorSection.MailFromName,
                errorSection.MailTo,
                bcc,
                errorSection.MailSubjectPrefix + " " + ex.Message,
                s.ToString(),
                errorSection.Smtp,
                errorSection.SmtpPort,
                errorSection.SmtpSSL,
                errorSection.AuthenticationUser,
                errorSection.AuthenticationPwd
            );

            return true;
        }

        #endregion
    }
}
