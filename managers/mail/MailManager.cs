﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Generic;
using it.itryframework2.config;
using it.itryframework2.managers.config;
using it.itryframework2.interfaces;

namespace it.itryframework2.managers.mail
{
    public class MailManager
    {
        protected MailConfigurationSection _mailSection;

        protected MailManager ()
        {
            _mailSection = ConfigManager.getMailConfigValues();
        }

        protected static MailManager _instance;
        public static MailManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new MailManager();
                return _instance;
            }
        }

        public void Send(List<string> to, string subject, string body)
        {
            Send(to, string.Empty, subject, body);
        }

        public void Send(List<string> to, string bcc, string subject, string body)
        {
            Send(to, new List<string> { bcc }, subject, body);
        }

        public void Send(string from, string to, string subject, string body)
        {
            Send(from, new List<string> { to }, subject, body);
        }

        public void Send(string from, string to, string[] bcc, string subject, string body)
        {
            Send(from, new List<string> { to }, new List<string>(bcc), subject, body);
        }

        public void Send(string from, string to, string bcc, string subject, string body)
        {
            Send(from, new List<string> { to }, new List<string> { bcc }, subject, body);
        }

        public void Send(string to, string subject, string body)
        {
            Send(new List<string> { to }, string.Empty, subject, body);
        }

        public void Send(List<string> to, List<string> bcc, string subject, string body)
        {
            string from = _mailSection.MailFrom;
            string fromName = _mailSection.MailFromName;
            foreach (string _to in to)
                Send(from, fromName, _to, bcc, subject, body);
        }

        public void Send(string from, List<string> to, List<string> bcc, string subject, string body)
        {
            string fromName = _mailSection.MailFromName;
            foreach (string _to in to)
                Send(from, fromName, _to, bcc, subject, body);
        }

        public void Send(string from, List<string> to, string subject, string body)
        {
            string fromName = _mailSection.MailFromName;
            foreach (string _to in to)
                Send(from, fromName, _to, null, subject, body);
        }

        public void Send(string from, string fromName, string to, List<string> bcc, string subject, string body, string smtp = null, int? port = null, bool? ssl = null, string username = null, string password = null)
        {
            string _from = string.IsNullOrWhiteSpace(from) ? _mailSection.MailFrom : from;
            string _fromName = string.IsNullOrWhiteSpace(fromName) ? _mailSection.MailFromName : fromName;
            string _username = string.IsNullOrWhiteSpace(username) ? _mailSection.AuthenticationUser : username;
            string _password = string.IsNullOrWhiteSpace(password) ? _mailSection.AuthenticationPwd : password;
            string _smtp = string.IsNullOrWhiteSpace(smtp) ? _mailSection.Smtp : smtp;
            int _port = port.HasValue ? port.Value : int.Parse(_mailSection.SmtpPort);
            bool _ssl = ssl.HasValue ? ssl.Value : _mailSection.SmtpSSL == "1";

            DoSend(_from, _fromName, to, bcc, subject, body, _smtp, _port, _ssl, _username, _password, _mailSection.SmtpClientProps);
        }

        protected void DoSend (string from, string fromName, string to, List<string> bcc, string subject, string body, string smtp, int port, bool ssl, string username, string password, string props)
        {
            if (!string.IsNullOrWhiteSpace(_mailSection.SmtpClientClassName))
            {
                ISmtpClient client = Activator.CreateInstance(Type.GetType(_mailSection.SmtpClientClassName)) as ISmtpClient;
                if (client != null)
                {
                    client.DoSend(from, fromName, to, bcc, subject, body, smtp, port, ssl, username, password, props);
                }
            }

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = smtp;
            smtpClient.Port = port;
            smtpClient.EnableSsl = ssl;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(username, password);
            }

            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(from, fromName);
                message.To.Add(new MailAddress(to.ToLower()));
                if (bcc != null)
                {
                    foreach (string _bcc in bcc)
                    {
                        if (!string.IsNullOrWhiteSpace(_bcc))
                            message.Bcc.Add(new MailAddress(_bcc));
                    }
                }

                message.Subject = subject;
                message.BodyEncoding = Encoding.UTF8;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

                message.AlternateViews.Add(htmlView);

                smtpClient.Send(message);
            };
        }
    }
}